plugins {
    id("com.android.library")
    kotlin("multiplatform") version "1.5.30"
    kotlin("plugin.serialization") version "1.4.32"
    id("maven-publish")
}

group = "me.kacper.barszczewski"
version = "1.0.0"

val serializationVersion = "1.1.0"
val coroutinesVersion = "1.4.3"
val nettyVersion = "4.1.68.Final"

repositories {
    google()
    mavenCentral()
    jcenter()
    maven("https://dl.bintray.com")
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    android()
    sourceSets {
        val commonJvmAndroid = create("commonJvmAndroid") {
            dependencies {
                implementation("com.github.aakira:napier:1.5.0-alpha1")

                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")

                implementation("io.netty:netty-all:$nettyVersion")

                
            }
        }
        val commonJvmAndroidTest = create("commonJvmAndroidTest") {
            dependsOn(commonJvmAndroid)
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesVersion")
            }
        }
        val jvmMain by getting {
            dependsOn(commonJvmAndroid)
        }
        val jvmTest by getting {
            dependsOn(commonJvmAndroid)
            dependsOn(commonJvmAndroidTest)
        }
        val androidMain by getting {
            dependsOn(commonJvmAndroid)
            dependencies {
                implementation("com.google.android.material:material:1.2.1")
            }
        }
        val androidTest by getting {
            dependsOn(commonJvmAndroidTest)
            dependencies {
                implementation("junit:junit:4.13")
            }
        }
    }
    android {
        publishLibraryVariantsGroupedByFlavor = true
        publishLibraryVariants("release", "debug")
    }
}

android {
    compileSdkVersion(29)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
//        applicationId = "me.kacper.barszczewski.library"
        minSdkVersion(24)
        targetSdkVersion(29)
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}