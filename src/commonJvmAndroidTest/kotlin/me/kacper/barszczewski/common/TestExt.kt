package me.kacper.barszczewski.common

import com.github.aakira.napier.Napier
import org.junit.AfterClass
import org.junit.BeforeClass
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

abstract class TestExt {

    data class Locker(
        val lock: ReentrantLock = ReentrantLock(),
        val condition: Condition = lock.newCondition(),
        var signalQueue: Int = 0
    ) {

        fun signalAfter(run: Runnable) {
            lock.withLock {
                run.run()
                signalQueue++
                condition.signalAll()
            }
        }

        fun await() {
            lock.withLock {
                if (signalQueue == 0) {
                    condition.await()
                }
                signalQueue--
            }
        }

        fun signal() {
            lock.withLock {
                signalQueue++
                condition.signalAll()
            }
        }
    }

    companion object {

        @BeforeClass
        @JvmStatic
        fun beforeTest() {
            Napier.base(ConsoleAntilog())
            println("#".repeat(50))
        }

        @AfterClass
        @JvmStatic
        fun afterTest() {
            println("#".repeat(50))
        }

        private val locks: MutableMap<Long, Locker> = mutableMapOf()

        private fun getLock(ID: Long): Locker = locks.getOrPut(ID) { Locker() }
        fun signalAfter(id: Long, run: Runnable) = getLock(id).signalAfter(run)
        fun await(id: Long) = getLock(id).await()
        fun signal(id: Long) = getLock(id).signal()

    }

}