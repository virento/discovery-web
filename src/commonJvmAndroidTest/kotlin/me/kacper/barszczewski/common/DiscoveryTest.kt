package me.kacper.barszczewski.common

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import me.kacper.barszczewski.common.client.DiscoveryClient
import me.kacper.barszczewski.common.ext.ConfigExt
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.locks.ReentrantLock
import kotlin.test.assertTrue

@Suppress("LocalVariableName")
@ExperimentalCoroutinesApi
class DiscoveryTest : TestExt() {

    private val sequential = ReentrantLock()

    @Before
    fun beforeEach() {
        sequential.lock()
    }

    @After
    fun afterEach() {
        sequential.unlock()
    }

    @Suppress("DeferredResultUnused")
    @Test(timeout = 10_000L)
    fun serverClientTest() {
        val ID = 1L

        val server = DiscoveryServer()
        server.startServerAsync()

        val channel = DiscoveryClient(2).sendRequest()

        GlobalScope.launch {
            val completed = channel.receive()
            assertTrue { completed.port == Config.APP_PORT }
            assertTrue { completed.hostname.toString().isNotEmpty() }
            signal(ID)
        }
        await(ID)
    }

    @Suppress("DeferredResultUnused")
    @Test(timeout = 60_000L)
    fun configLoadTest() {
        val ID = 2L
        Config.load(ConfigExt())
        val server = DiscoveryServer()
        server.startServerAsync()

        val channel = DiscoveryClient(2).sendRequest()

        GlobalScope.launch {
            val completed = channel.receive()
            assertTrue { completed.port == ConfigExt().APP_PORT }
            assertTrue { completed.appName == ConfigExt().APP_NAME }
            assertTrue { completed.hostname.toString().isNotEmpty() }
            signal(ID)
        }
        await(ID)
    }

    @Suppress("DeferredResultUnused")
    @Test(
        timeout = 60_000L
    )
    fun secondTry() {
        val ID = 3L
        val APP_NAME_TEST = "SuperDuper"

        Config.load(object {
            val SERVER_PORT = 28876
            val WAIT_TIME = 1_000
            val APP_NAME = APP_NAME_TEST
        })

        val server = DiscoveryServer()

        val channel = DiscoveryClient(2).sendRequest()
        GlobalScope.launch {
            delay(Config.WAIT_TIME + 100)
            server.startServerAsync()
            val completed = channel.receive()
            delay(100)
            assertTrue { completed.port == Config.APP_PORT }
            assertTrue { completed.appName == APP_NAME_TEST }
            assertTrue { completed.hostname.toString().isNotEmpty() }
            signal(ID)
        }
        await(ID)
    }

}
