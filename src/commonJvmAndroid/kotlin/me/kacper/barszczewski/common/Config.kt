package me.kacper.barszczewski.common

import java.util.*


class Config private constructor() {

    private var extClass: Any? = null

    companion object {

        private val config = Config()

        fun load(instance: Any) {
            config.extClass = instance
        }

        /**
         * Server
         */
        val SERVER_PORT
            get() = fromExt("SERVER_PORT") ?: 5768

        /**
         * Client
         */
        val WAIT_TIME
            get() = fromExt("WAIT_TIME") ?: 5_000L // Wait for response from server
        val BROADCAST_HOSTNAME
            get() = fromExt("BROADCAST_HOSTNAME") ?: "255.255.255.255"


        /**
         * APP
         */
        val APP_NAME
            get() = fromExt("APP_NAME") ?: "Discovery-WEB"
        val APP_ADDRESS: String?
            get() = fromExt("APP_ADDRESS")
        val APP_PORT
            get() = fromExt("APP_PORT") ?: 5555

        private inline fun <reified T> fromExt(filedName: String): T? {
            if (config.extClass == null) {
                return null
            }
            val config = config.extClass!!
            val fields = config.javaClass.declaredFields
            if (!fields.map { it.name }.contains(filedName)) {
                return null
            }
            val declaredField = config.javaClass.getDeclaredField(filedName)
            val get: Optional<Any>
            try {
                declaredField.isAccessible = true
                get = Optional.of(declaredField.get(config))
                declaredField.isAccessible = false
            } catch (e: Exception) {
                return null
            }
            if (get.isPresent) {
                val returnValue = get.get()
                return if (returnValue is T) returnValue else null
            }
            return null
        }
    }
}