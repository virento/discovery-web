package me.kacper.barszczewski.common

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.socket.DatagramPacket
import me.kacper.barszczewski.common.pojo.DiscoveryRequest
import me.kacper.barszczewski.common.pojo.DiscoveryResponse
import me.kacper.barszczewski.common.pojo.PacketSerializer

class DiscoveryServerHandler : SimpleChannelInboundHandler<DatagramPacket>() {

    override fun channelReadComplete(ctx: ChannelHandlerContext?) {
        ctx?.flush()
    }

    override fun channelRead0(ctx: ChannelHandlerContext?, msg: DatagramPacket?) {
        msg?.content()?.let { PacketSerializer.tryToCast<DiscoveryRequest>(it) }?.let {
            ctx?.writeAndFlush(
                DatagramPacket(
                    DiscoveryResponse(it.index, Config.APP_ADDRESS, Config.APP_PORT).toByte(),
                    msg.sender()
                )
            )
        }
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        cause?.printStackTrace()
    }
}