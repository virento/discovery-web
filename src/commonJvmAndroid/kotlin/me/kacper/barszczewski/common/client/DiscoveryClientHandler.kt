package me.kacper.barszczewski.common.client

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.socket.DatagramPacket
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import me.kacper.barszczewski.common.Config
import me.kacper.barszczewski.common.pojo.DiscoveredApp
import me.kacper.barszczewski.common.pojo.DiscoveryResponse
import me.kacper.barszczewski.common.pojo.PacketSerializer
import me.kacper.barszczewski.common.pojo.ResponseInfo

class DiscoveryClientHandler(private val callback: Channel<ResponseInfo>) :
    SimpleChannelInboundHandler<DatagramPacket>() {


    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        cause?.printStackTrace()
        ctx?.close()
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext?) {
        ctx?.close()
    }

    override fun channelRead0(ctx: ChannelHandlerContext?, msg: DatagramPacket?) {
        msg?.content()?.let {
            PacketSerializer.tryToCast<DiscoveryResponse>(it)?.let {
                GlobalScope.launch {
//                    callback.send(DiscoveredApp(Config.APP_NAME, msg.sender().address, Config.APP_PORT))
                    callback.send(ResponseInfo(it, msg.sender().address))
                }
            }
        }
    }
}