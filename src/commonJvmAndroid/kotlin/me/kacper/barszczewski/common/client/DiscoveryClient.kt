package me.kacper.barszczewski.common.client

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.DatagramPacket
import io.netty.channel.socket.nio.NioDatagramChannel
import io.netty.handler.logging.LogLevel
import io.netty.handler.logging.LoggingHandler
import io.netty.util.internal.SocketUtils
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import me.kacper.barszczewski.common.Config
import me.kacper.barszczewski.common.pojo.DiscoveredApp
import me.kacper.barszczewski.common.pojo.RequestInfo
import me.kacper.barszczewski.common.pojo.ResponseInfo
import me.kacper.barszczewski.common.utils.logger.LoggerWrapper

class DiscoveryClient(
    val numberOfTries: Int = 1,
    destAddress: List<String> = listOf(Config.BROADCAST_HOSTNAME)
) {

    private val requests = destAddress.map { RequestInfo(it, destAddress.indexOf(it)) }.toMutableSet()

    private val logger = LoggerWrapper()

    private var shouldStop = false

    lateinit var connectAsync: Deferred<Unit>
    lateinit var discoveredChannel: Channel<DiscoveredApp>
    lateinit var nioEventGroup: NioEventLoopGroup

    fun sendRequest(): Channel<DiscoveredApp> {
        discoveredChannel = Channel()
        connectAsync = tryToConnect(discoveredChannel)
        return discoveredChannel
    }

    fun stop() {
        shouldStop = true
        if (!nioEventGroup.isShuttingDown && !nioEventGroup.isShutdown) {
            nioEventGroup.shutdownGracefully()
        }
        discoveredChannel.close()
        connectAsync.cancel()
        GlobalScope.launch {
            delay(1_000)
            if (!nioEventGroup.isShutdown) {
                nioEventGroup.shutdown()
            }
        }
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    private fun tryToConnect(signalChannel: Channel<DiscoveredApp>) = GlobalScope.async {
        nioEventGroup = NioEventLoopGroup()

        val innerChannel: Channel<ResponseInfo> = Channel()

        try {
            val b = Bootstrap()
            b.group(nioEventGroup)
                .channel(NioDatagramChannel::class.java)
                .option(ChannelOption.SO_BROADCAST, true)
                .handler(object : ChannelInitializer<NioDatagramChannel>() {
                    override fun initChannel(ch: NioDatagramChannel) {
                        ch.pipeline().addLast(
                            LoggingHandler(LogLevel.INFO),
                            DiscoveryClientHandler(innerChannel)
                        )
                    }
                })

            GlobalScope.launch {
                while (!signalChannel.isClosedForReceive) {
                    val response = innerChannel.receive()
                    signalChannel.send(response.toDiscoveryApp())
                    synchronized(requests) {
                        requests.removeIf {
                            it.index == response.index
                        }
                    }
                }
            }

            val channel = b.bind(0).sync().channel()
            for (i in 0 until numberOfTries) {
                logger.debug { "Sending Discovery package..." }
                synchronized(requests) {
                    requests.forEach {
                        channel.writeAndFlush(
                            DatagramPacket(
                                it.toDiscoveryRequest().toByte(),
                                SocketUtils.socketAddress(it.destAddress, Config.SERVER_PORT)
                            )
                        )
                    }
                }
                delay(Config.WAIT_TIME)
                if (shouldStop) {
                    break
                }
            }
        } finally {
            if (!nioEventGroup.isShutdown && !nioEventGroup.isShuttingDown) {
                nioEventGroup.shutdownGracefully()
                signalChannel.close()
            }
        }
    }

}