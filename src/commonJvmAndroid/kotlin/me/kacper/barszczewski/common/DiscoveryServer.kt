package me.kacper.barszczewski.common

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioDatagramChannel
import io.netty.handler.logging.LogLevel
import io.netty.handler.logging.LoggingHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.lang.Exception
import java.util.concurrent.TimeUnit

class DiscoveryServer() {

    private val nioGroup = NioEventLoopGroup()

    fun startServer() {
        run()
    }

    fun startServerAsync() = GlobalScope.async { run() }


    fun run() {
        try {
            val b = Bootstrap()
            b.group(nioGroup)
                .channel(NioDatagramChannel::class.java)
                .option(ChannelOption.SO_BROADCAST, true)
                .handler(object : ChannelInitializer<NioDatagramChannel>() {
                    override fun initChannel(ch: NioDatagramChannel) {
                        ch.pipeline().addLast(
                            LoggingHandler(LogLevel.INFO),
//                            ObjectEncoder(),
//                            ObjectDecoder(ClassResolvers.cacheDisabled(null)),
                            DiscoveryServerHandler(),
                        )
                    }
                })
            b.bind(Config.SERVER_PORT).sync().channel().closeFuture().await()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            nioGroup.shutdownGracefully()
        }
    }

    fun close() {
        nioGroup.shutdownGracefully(2, 5, TimeUnit.SECONDS)
    }

}