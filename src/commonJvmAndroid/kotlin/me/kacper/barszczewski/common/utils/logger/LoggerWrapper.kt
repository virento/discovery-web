package me.kacper.barszczewski.common.utils.logger

import com.github.aakira.napier.Napier

class LoggerWrapper {
    fun info(message: () -> String) {
        Napier.i(message)
    }

    fun debug(message: () -> String) {
        Napier.d(message)
    }

    fun debug(exception: Exception, message: () -> String) {
        Napier.d(message, exception)
    }

    fun warn(exception: Exception, message: () -> String) {
        Napier.w(message, exception)
    }

    fun error(exception: Exception, message: () -> String) {
        Napier.e(message, exception)
    }

    fun error(message: () -> String) {
        Napier.e(message)
    }

    fun warn(message: () -> String) {
        Napier.w(message)
    }

    fun verbose(message: () -> String) {
        Napier.v(message)
    }

}