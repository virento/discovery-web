package me.kacper.barszczewski.common.pojo

import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable

abstract class PacketSerializer: Serializable {

    fun toByte(): ByteBuf {
        val baos = ByteArrayOutputStream()
        val oos = ObjectOutputStream(baos)
        oos.writeObject(this)
        return Unpooled.wrappedBuffer(baos.toByteArray())
    }

    companion object {

        inline fun <reified T : PacketSerializer> tryToCast(content: ByteBuf): T? {
            val buff = ByteArray(content.readableBytes())
            content.readBytes(buff)
            val bais = ByteArrayInputStream(buff)
            val ois = ObjectInputStream(bais)
            val obj = ois.readObject()
            if (obj is T) {
                return obj
            }
            return null
        }

    }

}