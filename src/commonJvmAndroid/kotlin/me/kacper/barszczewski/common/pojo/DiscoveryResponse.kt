package me.kacper.barszczewski.common.pojo

data class DiscoveryResponse(
    val index: Int,
    val ipAddress: String?,
    val port: Int
) : PacketSerializer()