package me.kacper.barszczewski.common.pojo


data class RequestInfo(
    val destAddress: String,
    val index: Int,
    val received: Boolean = false
) {

    fun toDiscoveryRequest() = DiscoveryRequest(
        index
    )

}