package me.kacper.barszczewski.common.pojo

import java.net.InetAddress

data class DiscoveredApp(
    val appName: String,
    val hostname: InetAddress,
    val port: Int
)