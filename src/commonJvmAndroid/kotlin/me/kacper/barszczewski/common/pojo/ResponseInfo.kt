package me.kacper.barszczewski.common.pojo

import me.kacper.barszczewski.common.Config
import java.net.InetAddress

data class ResponseInfo(
    val discoveryResponse: DiscoveryResponse,
    val senderAddress: InetAddress
) {

    val index = discoveryResponse.index

    fun toDiscoveryApp() = DiscoveredApp(
        Config.APP_NAME,
        senderAddress,
        Config.APP_PORT
    )
}