package me.kacper.barszczewski.common.pojo

import me.kacper.barszczewski.common.Config

data class DiscoveryRequest(
    val index: Int,
    val message: String = "me.kacper.barszczewski.discovery-web",
    val appName: String = Config.APP_NAME
) : PacketSerializer()